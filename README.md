Bir Yazılımı Kırmak - Cracking

---

Bu sayfada, birçok kişinin bildiği belli bir süre ücretsiz kullanımının ardından kullanımının lisans gerektirdiği bir yazılımı kırarak (`crack`) bu tür lisans gerektiren yazılımların nasıl kırıldığı ile alakalı bilgiler eğitim amacıyla bulunmaktadır.

Bazı yerler farklı sebeplerle yanlışlık içeriyor olabilir. Düzenleme yapmaktan çekinmeyin. İletişim için [Telegram](https://t.me/tayyizaman)

Bu yazının gitlab reposu için [tıklayınız](https://gitlab.com/enes1313/bir-yazilimi-kirmak-cracking).

**İçerik**  

- [Ne nedir](#ne-nedir)
- [Hazırlık](#hazırlık)
- [Keygen Yapalım](#keygen-yapalım)
- [Patch Yapalım](#patch-yapalım)
- [8 Bit Müzik Keyfi](#8-bit-müzik-keyfi)
- [Kaynak Kod](#kaynak-kod)

## Ne nedir

![Cracking](pictures/index.png)

Bilgisayar dünyasında bir yazılımın kırılması (`cracking`), bir veya birilerinin (`cracker(s)`) yazılımın üreticileri tarafından istenmeyen özellikleri (kopyalamaya karşı korunma, değişiklik, lisanssız kullanım vb.) belirli yöntemlerle kaldırmak veya devre dışı bırakmak için yazılımın değiştirilmesi veya atlatılmasıdır. Bu yöntemler lisans anahtarlarının bir şekilde çalınması suretiyle kullanmaktan ilgili yazılım dosyasının içeriğinde düşük seviye işlemler yapmaya kadar geniş ölçüde olabilir.

Bu işlemler sonucunda `cracker`'ın çıkardığı ve diğer kişilerce kullanılabilen araçlar genellikler `keygen`, `patch` ve `loader` isimlerine sahiptir. Örneğin `Windows 7 Loader`, `Sony Vegas Pro Keygen`, `Adobe Patcher` gibi yazılımlar internette sıklıkla dolaşımdadır.

`Keygen`, seri numaraları üreten bir yazılımdır. `Patch`, ilgili yazılımın makine kodunu değiştiren bir yazılımdır. `Loader`, bir yazılımın ​​akışını değiştirir ve korumayı kaldırmadan atlatmaya yarar.

`Cracking`'de ünlenmiş kendi kültürlerini oluşturmuş bir çok isim ve ekip bulunmaktadır. Bilinen birkaç ekip ismi vermek gerekirse `Razor1911`, `Skidrow`, `Fairlight` ve `Reloaded` örnek olarak verilebilir.

`Cracking`'e genelde sanat gözüyle de bakılır. Yapım aşaması bir yana, üretilen yazılımlarda ilginç grafikler, sesler ve yazıların kullanılması ile bir kültür oluşmuştur. Lise döneminde bazı keygenleri sırf dinlemek için açtığımı biliyorum. Çoğu 8 bit müzik olan bu müzikleri siz de dinlemek isterseniz [buraya](http://keygenmusic.net) tıklamanız yeterli.

`Patch` ve `loader` yazılımları ilgili yazılımda sadece ilgili amaçla değişiklik yaparlar diyemeyiz. Zararlı yazılım parçalarını da ilgili yazılıma ekleyebilirler. `Patch` ve `loader` ve `keygen` yazılımlarının kendileri de zararlı yazılım olabilmektedir. Bu konuda ücret ve güven arasında bir seçim gerekebilir. Burada başka bir soru da akla gelmiyor değil. Peki neden kapalı kaynağa sahip ticari yazılımlara güvenelim ki? Onlar da bir nedenden dolayı bizler için zararlı olabilirler. Bazı `cracker`'lar sırf bu nedenle `crack` yapmaktadırlar. Bazıları ise ücretin çok yüksek olduğunu eleştirerek yapmaktadırlar.

Tabii olarak bir de etik taraf vardır. Emek hırsızlığı kategorisine girdiğinden çoğu ülkede `crack` ve `crack`li ürün dağıtımı yasa dışıdır. Bununla birlikte tersine mühendislik ve `cracking` için eğitim kaynakları yasal olmaktadır. Bu yazı da bu kategoriden olaya dahil oluyor :) Bu yazı, kıracağımız yazılımın ismini cismini kullanmadan bir basit eğitime sahiplik etmektedir.

## Hazırlık

Bir kullanım lisansı gerektiren yazılımların çoğunun kaynak kodu bulunmamaktadır. Hangi tür yazılımda ne tür bir yöntem kullanılacağı da değişiklik göstermektedir. Bu öğreticide seçilen yazılım, windows işletim sisteminde çalışan çoğu kişinin bildiği kullandığı 32 bit bir yazılımdır. Yazılımın belirli bir süre ücretsiz kullanımı bulunmaktadır. Süre dolduğunda ise seri numara (`serial number`) girerek kayıt olmamız istenmektedir. Bu aşamada kayıt olmazsak yazılımı hiç bir şekilde kullanamamaktayız. Ücretsiz kullanım süresi boyunca bazı ek özellikler de kapalı durumdadır. Yazılımın kayıt pencresinde yazılım bizden kullanıcı adı ve soyadı, e-mail ve seri numarası istemektedir.

Bu yazılımın akışını incelemek için bir `debugger` yazılımına ihtiyacımız var. Bir `debugger`'ın ana amacı, yazılım çalışırken yazılımın devam eden işlemlerini izlenmesine ve bilgisayar kaynaklarındaki değişiklikleri (bellek, register vb.) izlenmesine olanak vermesidir. Bu yazıda, `debugger` olarak `x64dbg` yazılımı ile gelen `x32dbg` alt yazılımı kullanılacaktır. Bu yazılım eskiden sıkça kullanılan `Olldbg` yazılımının modern hâlidir diyebiliriz.

Yazılımı bu şekilde analiz ederken kesinlikle biraz da olsa bilinmesi gereken dil, `ASM` dilidir. Windows altında 32 bit bir yazılımı `debug` edeceğimizden `x86 ASM` bu yazı için iş görecektir. İleri derecede bilmek şart değildir. Burada bilinmesi gereken, `ASM` dilini okuyabilmektir. 

Windows altında olunduğundan `WinAPI` bilgisi de gerekecektir. Örneğin, `WinAPI`'de fonksiyon çağrılarında parametreler `stack`'e (`yığın`) yüklenmekte, fonksiyon geri dönüş değeri ise `eax` registerına (`yazmaç`) yazılmaktadır.

Ek olarak ağ analizi yapmak için `Wireshark` yazılımı da gerekecektir. Kırılacak yazılım için bu araç da kullanılacaktır.

Bunun dışında farklı yazılımlar ve amaçlar için farklı araçlar tabii olarak bulunmaktadır lâkin bu yazıda kullanılmayacaktır.

Tüm süreçte `x32dbg`, `wireshark`, `nodepad++`, `mingw64/clang` yazılımları kullanılacaktır.

Not 1: Yazılımları debugger ile çalışmasını kolayca takip edemeyebiliriz. Örneğin, paketleme yapılmış olabilir. Paketleme araçları (örneğin `VMProtect`, `UPX`) ile paketlenmiş mi diye `PEiD` yazılım aracı kullanılabilir. Bu konu ve debug yöntemlerini zorlaştıracak diğer konular bu yazıda değinilmeyecektir. Kıracağımız yazılımda böyle durumlar bulunmamaktadır. 

Yazılımda `PEiD` çıktısından paketleme olmadığı aşağıda görülmektedir.

![PEiD ekran görüntüsü](pictures/peid_ss.png)

Not 2: Kırılacak yazılımdan `NS` olarak bahsedilecektir.

## Keygen Yapalım

`NS` yazılımının internet yokken kayıt ekranınına gelelim. Aşağıda görüldüğü üzere `First Name`, `Last name`, `Email`, `Serial number` bölümleri yer almaktadır.

![NS kayıt ekranı](pictures/ns_ss_registration.png)

Birşey yazmadan `OK` butonuna basıyoruz. Aldığımız hata aşağıda yer almaktadır.

![İsim hata penceresi](pictures/ns_ss_registration_error_first_name.png)

`First Name` alanına sadece 'f' harfini yazdıktan sonra `OK` butonuna basıyoruz. Aldığımız hata aşağıda yer almaktadır.

![Soyad hata penceresi](pictures/ns_ss_registration_error_last_name.png)

`Last name` alanına sadece 'l' harfini ve  `Email` alanına 'e' yazdıktan sonra `OK` butonuna basıyoruz. Aldığımız hata aşağıda yer almaktadır.

![Seri numara hata penceresi](pictures/ns_ss_registration_error_serial_number.png)

`Serial Number` alanına 's' harfini yazsak da "dsfdsfdf" yazsak da hep hata almaktayız. Aldığımız hatayı göstermeyeceğim. Çünkü `NS` ile isimlendirdğimiz bu yazılımı fazla deşifre etmeyelim :)

Buraya kadar baya bir bilgi çıkarttık aslında. İsim soy isim ve e-mail alanları belirli bir formata göre filtre içermemekte ve sadece bu alanlar boş mu değil mi diye kontrol yapılmaktadır. `Serial Number` alanı ise belirli bir formata sahip ki şuanlık bilmiyoruz. Yazılım, internetin olmadığını bize uyarı olarak vermedi. Buradan yola çıkarak belki yerel olarak kontrol sağlanacağını düşünüyoruz. Bu nedenle `Keygen` yapabileceğimizi düşünüyoruz. Bu bilgilerle bir senaryoya koyulabiliriz. 

Öncelikle `NS` yazılımını `x32dbg` yazılımı ile açacağız. Çalışır duruma getireceğiz. Sonra kayıt ekranında aldığımız hata mesajlarını `x32dbg` yazılımında aratıp, nerelerde kullanıldığını inceleyeceğiz ki kayıt ekranını işleyen fonksiyonu bulabilelim. Bu fonksiyon başka yöntemlerle de bulunabilir. Bizim şuanlık yöntemimiz bu olacak. Peki neden böyle bir fonksiyon arıyoruz? Çünkü, hata mesajını gösteren fonksiyonun içinde veya fonksiyondan sonra yüksek ihtimal `Serial Number` kontrol yeri de olacak ve biz de oradaki `ASM` komutlarını okuyarak `keygen` yapabileceğiz.

Aşağıda `NS` yazılımının `x32dbg` yazılımında açıldıktan sonra gelen ekran görüntüsü görünmektedir. 

![x32dbg ekran görüntüsü](pictures/x32dbg_ns_run_1_ntdll.png)

Bir kez `x32dbg` yazılımında `Run` butonuna basınca aşağıda görüldüğü gibi `Entrypoint` noktasına gelmekteyiz.

![NS entrypoint](pictures/x32dbg_ns_run_2_ns.png)

Buradan da bir kez `Run` butonuna basınca `NS` yazılımı çalışmaktadır. `x32dbg` yazılımında aşağıdaki görselde de görüleceği üzere cpu penceresinde sağ tıklayıp `Search for`-> `Current Module` -> `String References` seçeneğine tıklayalım.

![String referanslar alanı](pictures/x32dbg_ns_search_for_string_refs.png)

Çıkan pencerede en aşağıda arama bölümüne ilk aldığımız hata olan "Plase enter your first name" yazısını yazalım. Aşağıda çıktılar görülmektedir.

![String arama sonuç ekranı](pictures/x32dbg_ns_refs.png)

Burada çift tıklayarak bu yazının nerelerde referans edildiği görülecektir. Tek tek baktığımızda 4. seçenekteki yer nokta atışı aradığımız fonksiyona bizi götürecektir. Aşağıda 4. referansın olduğu yerden kesit görünmektedir.

![Aranılan fonksiyon](pictures/x32dbg_ns_the_function.png)

İşte burada `ASM` dilini okumak devreye giriyor. Parça parça bu aşamada neler karşımıza geldiğini açıklayacağım.

Öncelikle bu fonksiyonun başına giderek neler olduğuna bakıyoruz. Bazı push işlemleri bellek adreslerinin edinimi gibi küçük şeyler dışında karşımıza pek birşey çıkmıyor. Sadece belirli bir adrese toplam 36 bayt yazılıyor. Yazılan baytlar ingiliz alfabesinin büyük harfleri ile 10 rakamdan oluşur ve sıralaması karışıktır. Buna benzer bir C dizisi oluşturursak bu *char array[] = "WYOP3B2VU7XM8CNR9E5T6SA0IZ4KLFQGHDJ1";* tanımı olabilir (Buradaki sıralama yazılımdaki sıralamadan farklıdır). Bunun ne için kullanılacağı tahmin edilebilir ama yeri geldiğinde ele alınacaktır.

Bu kopyalamalardan sonra aşağıdaki ASM kodu karşımıza çıkmaktadır.

```asm

Address  | Bytes                                     | ASM                                 |

00217177 | 6A 32                                     | push 32                             |
00217179 | 8D85 64010000                             | lea eax,dword ptr ss:[ebp+164]      |
0021717F | 50                                        | push eax                            |
00217180 | 68 B0040000                               | push 4B0                            |
00217185 | E8 1E0B0D00                               | call <NS.sub_2E7CA8>                |
0021718A | 85C0                                      | test eax,eax                        |
0021718C | 75 3E                                     | jne NS.2171CC                       |

```

5. satırda `call` ile fonksiyon çağrımı gerçekleşmektedir. Bu fonksiyonun parametreleri 1., 3. ve 4. satırlarda stack'e push ile gönderilmiştir. 6. satırda bu fonksiyonun geri dönüş değeri test edilmektedir. 6. ve 7. satırdaki durumda yapılmak istenen aslında eax eğer 0 dan farklı ise atla, değilse atlama demekten başka bir şey değildir. Atlama yeri başka bir yerde olduğu için onu buraya eklemedik. Birazdan o kısma dallanacağız. Yukarıdaki `ASM` kodunun benzer C karşılığını yazalım.

```c

char str[0x33]; // Tahmin

int ret = func1(0x4B0, str, 0x32);

if (0 == ret)
{
    // ...
}
else
{
    // ...
}

```

C karşılığına bakıp biraz daha anladıktan sonra, şimdi `eax` yazmacına 0 geldiğini düşünelim. `jne` den dolayı atlanmayacak ve bir sonraki komuta devam edilecektir. Peki bir sonraki komutlarda acaba ne var? Tanıdık gelecek mi bakalım.

```asm

Address  | Bytes                     | ASM                                 | Açıklama

0021718E | 50                        | push eax                            |
0021718F | 68 68AD3600               | push NS.36AD68                      | 36AD68:"Pencere Başlığı"
00217194 | 8B0D 04FC4400             | mov ecx,dword ptr ds:[44FC04]       | 0044FC04:&"Please enter your first name"
0021719A | 51                        | push ecx                            |
0021719B | 8B57 20                   | mov edx,dword ptr ds:[edi+20]       |
0021719E | 52                        | push edx                            |
0021719F | E8 1C68F5FF               | call <NS.sub_16D9C0>                |

```

Görüldüğü üzere eğer `eax` yazmacıı sıfır ise çalıştırılan yer burasıdır. Peki `x32dbg` yazılımının açıklama kısmında gösterdiği yazılara bakarsak, bu yazılar ne zaman karşımıza çıkmaktaydı? `First Name` alanına bir şey yazmadığımızda karşımıza çıkmıştı. Buradaki fonksiyon çağrımına `breakpoint` koyarsak ve yazılımda kayıt pencresinde ilgili alanı boş bırakıp `OK` butonuna basarsak `x32dbg` yazılımı akışı, buradaki `breakpoint`te durduracaktır. Sadece `step over` tuşuna bastığımızda ise ekrana ilk başta aldığımız o hata penceresi gelecektir. Bundan sonra çalıştırılan birkaç `ASM` komutunu önemsiz kabul ediyoruz. Yukarıda `eax` yazmacın sıfırdan farklı gelmesi için `First Name` alanına yazı yazıyoruz. (denemelerden sonra `eax` yazmacına, "First Name" alanına girilen yazını uzunluğu yazıldığı görülmüştür.)

Bu `ASM` komutlarına benzer durum soyad, e-posta ve seri numara kısımları için yapılmaktadır. Aynı işlemler olduğu için bu yazıda anlatmayı es geçiyorum. Aynı zamanda "seri numarası boşlukla başlıyor mu sonunda boşluk var mı?" gibi durumları test eden kısımları da atlıyorum. Atladığımız kısımlardan bilmemiz gereken tek şey kayıt pencresinde girilen seri numarasının `ebp+1A4` adresine yazıldığıdır. 

Kayıt ekranındaki tüm alanları doldurup devam etmeyi unutmayın :)

Sonraki `ASM` komutları aşağıdadır.

```asm

// 1

002172F2 | 8D8D A4010000                             | lea ecx,dword ptr ss:[ebp+1A4]      |
002172F8 | 51                                        | push ecx                            |
002172F9 | E8 80261000                               | call <NS.sub_31997E>                |
002172FE | 83C4 04                                   | add esp,4                           |

// 2

00217301 | 8D85 A4010000                             | lea eax,dword ptr ss:[ebp+1A4]      |
00217307 | 8D50 01                                   | lea edx,dword ptr ds:[eax+1]        |
0021730A | 8D9B 00000000                             | lea ebx,dword ptr ds:[ebx]          |

// 3

00217310 | 8A08                                      | mov cl,byte ptr ds:[eax]            |
00217312 | 40                                        | inc eax                             |
00217313 | 84C9                                      | test cl,cl                          |
00217315 | 75 F9                                     | jne NS.217310                       |

// 4

00217317 | 2BC2                                      | sub eax,edx                         |
00217319 | 83F8 17                                   | cmp eax,17                          |
0021731C | 0F85 F4000000                             | jne NS.217416                       |

// 5

00217322 | 32DB                                      | xor bl,bl                           |
00217324 | B0 2D                                     | mov al,2D                           | 2D:'-'
00217326 | 3885 A9010000                             | cmp byte ptr ss:[ebp+1A9],al        |
0021732C | 75 10                                     | jne NS.21733E                       |
0021732E | 3885 AF010000                             | cmp byte ptr ss:[ebp+1AF],al        |
00217334 | 75 08                                     | jne NS.21733E                       |
00217336 | 3885 B5010000                             | cmp byte ptr ss:[ebp+1B5],al        |
0021733C | 74 02                                     | je NS.217340                        |
0021733E | B3 01                                     | mov bl,1                            |

```

Kısaca bu kısımları anlatmak gerekirse;

1. parçada `ebp+1A4` adresi yani seri numaranın olduğu adres `ecx` yazmacına yazılır oradan da fonksiyon tarafından kullanılması için `push` ile yığına gönderilir. Burada ne olduğunu `x32dbg` yazılımında adım adım giderek öğrenebiliyoruz. 1. satıra sağ tıkladıkta sonra "Follow in Dump" alanına gelip oradan da Address: `EBP + 1A4` kısmını seçersek aşağı pencerede `Dump 1` alanında seri numarasını göreceğiz. `x32dbg` yazılımında yukarıdaki 1. parçanın 3. satırına kadar adım adım çalıştırarak geldikten sonra bir kez daha bir adım daha çalıştırırsak, `Dump 1` alanında bulunan seri numarasınında küçük karakterler büyük harfe dönüştüğü görülecektir. Bu sayede seri numarasında küçük harf ve büyük harf arasında ayrım olmadığını anlamış oluyoruz. Küçük karakterler girildiğinde, yazılım bu harfleri büyük harflere dönüştürmektedir.

2. parçada `eax`, `edx` yazmaçlarına sırasıyla seri numaranın olduğu yerin adresi ve o adresin bir fazlası yazılıyor. `ebx` yazmacına neden ne yapılıyor anlamadım :) Ölü kod gibi orası, başka bir yerden de atlama yok bu kısma :)

3. parçada seri numarasının bulunduğu bölgede gezerek içinde sıfır yani sonlandırıcı karakter görene kadar `eax` yazmacı arttırılıyor.

4. parçada `sub eax, edx` işlemi C dilindeki `eax = eax - edx;` ifadesi gibidir. Yani burada olan şey aslında eax registerına seri numaranın uzunluğunu hesaplamaktır. Sonra bu değer `0x17` değeri ile karşılaştırılmaktadır. Eğer bu değerden farklı ise yazılım "incorrect serial number" hata ekranını gösterticek şekilde yönlenir. O halde seri numaranın `0x17` yani 23 karakterden oluştuğu ortaya çıkar.

5. parçada seri numaranın belirli bölgelerinde '-' karakteri var mı yok mu diye tespit yapılıyor. Eğer yoksa başarısız bir lisans anahtarı olarak kabul edileceği deneyerek görülebilir. Aynı zamanda yazılım bunu `bl`'ye 1 atayarak gerçekleştiriyor. Yani `bl` bir ise yanlış bir seri numarası girildiğini yazılım ileriki kontrollerde anlayacak. `ebp+1A4` seri numarasının başlangıç adresi olduğuna göre `1A9`, `1AF` ve `1B5` değerleri incelendiğinde seri numaranın formatı `XXXXX-XXXXX-XXXXX-XXXXX` şeklinde olacağı anlaşılmaktadır. Yani 4 tane 5 karakter ve aralarında '-' karakteri olacak şekilde 23 karakterlik bir seri numarası istenmektedir.

Seri numarası alanına artık `XXXXX-XXXXX-XXXXX-XXXXX` formatında veri girerek ilerleyeceğiz. Bir sonraki yere `breakpoint` koyduktan sonra veri girişi ve `OK` butonu ve tekrar analize devam...

Bu komutlardan sonra gelen komutun bir kısmı biraz anlamsız gözükmetedir. Analiz ederken ilk başta neden yapıldığını anlamamıştım. Aşağıda paylaşacağım komutlar 4 defa çağrılan bir fonksiyon içermektedir. Parametre olarak iki adres ve 5 sayısını almaktadır. Aslında seri numarasının 4 bölümü teker teker bir tane fonksiyona gönderiliyor ve belirli bir kontrolden sonra bu bölümler bir başka adrese yazılıyor. Gereksiz gibi görünen bu komutlarda aslında yapılan şey seri numarasının ingiliz alfabesi ve 10 rakamı dışında bir karakter içermemesine yönelik çok ilginç bir algoritma. Birkaç tane karar ve döngü ile çözülebilecek bir durum enterasan bir yöntemle çözülmüş. Yazının konusu olmadığından ve içeriği incelenmenin gereksiz olduğu anlaşıldığından burada değinmeyeceğim. Çünkü biz zaten geçerli bir seri numarası üretmeye çalışıyoruz. Neden böyle bir durumun olduğu tahmini yaparsak belki bu yazılımı önceden kıran kişilerin yöntemlerini bozmak veya geçerli düzgün girdi almak için olabileceği aklıma gelmektedir. Çünkü bu komutlardan sonra gelen fonksiyonlarda farklı karakterin girilmesinde başarısızlık tespit edilebilmektedir. Sanırım unicode karakterden oluşabilecek garip durumlar engellenmiş de olabilir.

Bahsettiğimiz yeri öncelikle gösterelim.

```asm

00217340 | 6A 05                                     | push 5                              |
00217342 | 8D95 A4010000                             | lea edx,dword ptr ss:[ebp+1A4]      |
00217348 | 52                                        | push edx                            |
00217349 | 8D85 F0010000                             | lea eax,dword ptr ss:[ebp+1F0]      |
0021734F | 50                                        | push eax                            |
00217350 | E8 ABD00F00                               | call <NS.sub_314400>                |

00217355 | 6A 05                                     | push 5                              |
00217357 | 8D8D AA010000                             | lea ecx,dword ptr ss:[ebp+1AA]      |
0021735D | 51                                        | push ecx                            |
0021735E | 8D95 E8010000                             | lea edx,dword ptr ss:[ebp+1E8]      |
00217364 | 52                                        | push edx                            |
00217365 | E8 96D00F00                               | call <NS.sub_314400>                |

0021736A | 6A 05                                     | push 5                              |
0021736C | 8D85 B0010000                             | lea eax,dword ptr ss:[ebp+1B0]      |
00217372 | 50                                        | push eax                            |
00217373 | 8D8D D8010000                             | lea ecx,dword ptr ss:[ebp+1D8]      |
00217379 | 51                                        | push ecx                            |
0021737A | E8 81D00F00                               | call <NS.sub_314400>                |

0021737F | 6A 05                                     | push 5                              |
00217381 | 8D95 B6010000                             | lea edx,dword ptr ss:[ebp+1B6]      |
00217387 | 52                                        | push edx                            |
00217388 | 8D85 E0010000                             | lea eax,dword ptr ss:[ebp+1E0]      |
0021738E | 50                                        | push eax                            |
0021738F | E8 6CD00F00                               | call <NS.sub_314400>                |

```

Burada yukarıda dediğim gibi 4 defa `sub_314400` fonksiyonu çağrılıyor. Her fonksiyon çağrımında seri numaranın her 5 karakterlik bölümünün adresi ile birlikte bir başka adres parametre olarak yığına gönderiliyor. `Dump 1` alanından ilgili bu adreslere baktığımızda bu fonksiyonun çalıştırılması sonrası aslında sadece 5 baytın kopyalanması olduğu görülüyor. Farklı bir kontrol içerdiğinden, `sub_314400` fonksiyonunu içeriğini incelemeden C diline dönüştürdüğüm bir kısmını soru olarak sizle paylaşacağım.

```c

uint32_t val1 = /* ... */;
uint32_t val2 = val1 ^ 0xFFFFFFFF ^ (val1 + 0x7EFEFEFF);

if ((val2 & 0x81010100) != 0)
{
    puts("Hangi val1 degerleri buraya girmemize izin vermez?");
}

```

Bizim için önemli yerlerden birine geldik. Yukarıdaki gereksiz gibi görünen komutlardan sonra yürütülen aşağıda verilen komutlar yukarıda içeriği yazılan `ebp`'nin `1F0`, `1E8`, `1D8`, `1E0` ofsetlerini alarak içeriğinde bulunan 5 karakteri tekil bir sayısal değere dönüştürmektedir. Amacı açıklamak için bir örnek verirsek, bir uygulama sizden 4 rakam istiyorsa ve bunun başka bir zamanda alınan farklı bir 4 rakamla karışmamasını istiyorsanız ve bir benzersiz sayısal değer istiyorsanız yapmanız gerekenlerden biri 4 basamaklı sayı üretmektir. İlk verilen rakamı 1000 ile ikincisini 100 ile üçüncüsünü 10 ile dördüncüsünü direk kullanıp çıkan sonuçları toplarsanız bir sıralamada verilen 4 rakamdan üretilen değer bir başka sırada verilen 4 rakamdan ürettiğimiz değerle eş olmayacak. Eğer 4 rakamı sadece toplasaydık, örneğin 1, 1, 1, 2 rakamları ile 2, 1, 0, 2 rakamları aynı değeri üretecekti. Şimdi peki elimizde 10 rakam değil de 36 farklı değer varsa ne yapmalıyız? Her bir verilen değeri en az 36'nın kuvvetleri ile çarpıp toplamalıyız. Bu konunun mantığını ve algoritmasını size bırakıyorum.

```asm

00217394 | C685 F5010000 00                          | mov byte ptr ss:[ebp+1F5],0         |
0021739B | C685 ED010000 00                          | mov byte ptr ss:[ebp+1ED],0         |
002173A2 | C685 DD010000 00                          | mov byte ptr ss:[ebp+1DD],0         |
002173A9 | C685 E5010000 00                          | mov byte ptr ss:[ebp+1E5],0         |

002173B0 | 8D4D D4                                   | lea ecx,dword ptr ss:[ebp-2C]       |
002173B3 | 51                                        | push ecx                            |
002173B4 | 8D95 F0010000                             | lea edx,dword ptr ss:[ebp+1F0]      |
002173BA | 52                                        | push edx                            |
002173BB | E8 90ECFFFF                               | call <NS.sub_216050>                |
002173C0 | 83C4 38                                   | add esp,38                          |
002173C3 | 85C0                                      | test eax,eax                        |
002173C5 | 75 02                                     | jne NS.2173C9                       |
002173C7 | B3 01                                     | mov bl,1                            |

002173C9 | 8D45 E4                                   | lea eax,dword ptr ss:[ebp-1C]       |
002173CC | 50                                        | push eax                            |
002173CD | 8D8D E8010000                             | lea ecx,dword ptr ss:[ebp+1E8]      |
002173D3 | 51                                        | push ecx                            |
002173D4 | E8 77ECFFFF                               | call <NS.sub_216050>                |
002173D9 | 83C4 08                                   | add esp,8                           |
002173DC | 85C0                                      | test eax,eax                        |
002173DE | 75 02                                     | jne NS.2173E2                       |
002173E0 | B3 01                                     | mov bl,1                            |

002173E2 | 8D55 EC                                   | lea edx,dword ptr ss:[ebp-14]       |
002173E5 | 52                                        | push edx                            |
002173E6 | 8D85 D8010000                             | lea eax,dword ptr ss:[ebp+1D8]      |
002173EC | 50                                        | push eax                            |
002173ED | E8 5EECFFFF                               | call <NS.sub_216050>                |
002173F2 | 83C4 08                                   | add esp,8                           |
002173F5 | 85C0                                      | test eax,eax                        |
002173F7 | 75 02                                     | jne NS.2173FB                       |
002173F9 | B3 01                                     | mov bl,1                            |

002173FB | 8D4D C0                                   | lea ecx,dword ptr ss:[ebp-40]       |
002173FE | 51                                        | push ecx                            |
002173FF | 8D95 E0010000                             | lea edx,dword ptr ss:[ebp+1E0]      |
00217405 | 52                                        | push edx                            |
00217406 | E8 45ECFFFF                               | call <NS.sub_216050>                |
0021740B | 83C4 08                                   | add esp,8                           |
0021740E | 85C0                                      | test eax,eax                        |
00217410 | 74 04                                     | je NS.217416                        |
00217412 | 84DB                                      | test bl,bl                          |
00217414 | 74 16                                     | je NS.21742C                        |

```

Yukarıda da analttığımız üzere burada `ebp`'nin `1F0`, `1E8`, `1D8`, `1E0` ofsetlerini alarak sayısal değer üretilmektedir. Her bir seri numarası bölümü `sub_216050` fonksiyonuna gönderiliyor. Sırayla `ebp`'nin `1F0`, `1E8`, `1D8`, `1E0` ofsetlerine ise sayısal değerler yazılıyor. Peki sayısal değer yazıldığını nerden biliyoruz? Daha önceden `sub_216050` fonksiyonunun içeriğini bildiğim için :) Burada önemli olan `sub_216050` fonksiyonu `eax` yazmacına sıfırdan farklı değer yazması gerekmesi. Diğer türlü başarısız olarak ele alınması için `bl`'ye 1 yazılıyor. Eğer bir fonksiyon girilen değeri sayısal değere çeviriyorsa neden başarısız bir durumu olsun ki? İşte eğer buraya gelen seri numarası içerisinde ingiliz alfabesi ve 10 rakam harici bir karakter varsa burada başarısızlık oluşacak. Peki bu kontrol daha önce yapılmamış mıydı? Gibi, tam değil. Bizim için bu fonksiyon hiçbir zaman başarısız dönmeyecek çünkü geçerli bir seri numarası üreterek ilerlemeyi hedefliyoruz.

`sub_216050` fonksiyonunun `ASM` halini direk vermektense `1F0` yazılımında ilgili fonksiyona gittikten ve klavyeden `G` tuşuna bastıktan sonra ortaya çıkan grafik halini verelim.

![Sayısal değer üreten fonksiyonun grafik görünümü](pictures/x32dbg_ns_convert_number.png)

Görselde bulunan "3CF4C0" adresinde yukarıda bahsettiğimiz dizi bulunmaktadır. Bu dizi ingiliz alfabesi ve 10 rakamdan oluşmaktaydı. Görseldeki fonksiyonun C diline direk dönüştürülmüş hali aşağıdadır.

```c

int32_t sub_216050(int32_t *p_out, const char *p_serial)
{
    const char *F4C0 = "WYOP3B2VU7XM8CNR9E5T6SA0IZ4KLFQGHDJ1"; 
    int32_t eax;
    int32_t ecx = 0;
    const char *edi = p_serial;
    int32_t *esi = p_out;
    char dl;

    LABEL_1:

    eax = 0;
    dl = *(edi + ecx);

    LABEL_2:

    if (dl == *(CEF4C0 + eax))
    {
        if (eax != 0xFFFFFFFF)
        {
            *esi = *esi * 0x25 + eax;

            if (++ecx < 5)
            {
                goto LABEL_1;
            }
            else
            {
                eax = 1;
            }
        }
    }
    else
    {
        if (++eax < 0x24)
        {
            goto LABEL_2;
        }
        else
        {
            eax = 0;
        }
    }
        
    return eax;
}

```

Bizim için gereksiz olsun olmasın tüm gereksiz işlemleri çıkartıp bu kodun daha güzel bir C dili halini de paylaşalım:

```c

static void calculate_block(int32_t *p_block, const char *p_serial)
{
    static const int32_t idx_map[] = {
        23, 35, 6, 4, 26, 18, 20, 9, 12, 16, // 0 - 9
        0, 0, 0, 0, 0, 0, 0,                 // dummy
        22, 5, 13, 33, 17, 29, 31, 32, 24,   // A -...
        34, 27, 28, 11, 14, 2, 3, 30, 15, 
        21, 19, 8, 7, 0, 10, 1, 25           // ...- Z
    };
    static const int32_t powers_of_37[] = {1874161, 50653, 1369, 37, 1};

    *p_block = 0;
        
    for (int i = 0; i < 5; i++)
    {
        *p_block += powers_of_37[i] * idx_map[p_serial[i] - '0'];
    }
}

```

Evet, yukarıdaki ASM komutlarından sonra başarılı bir şekilde ilerleyince karşımıza gelen diğer `ASM` komutlarına bakalım.

```asm

0019742C | 8B4D D4                                   | mov ecx,dword ptr ss:[ebp-2C]       |
0019742F | B8 83BEA02F                               | mov eax,2FA0BE83                    |
00197434 | F7E9                                      | imul ecx                            |
00197436 | C1FA 03                                   | sar edx,3                           |
00197439 | 8BC2                                      | mov eax,edx                         |
0019743B | C1E8 1F                                   | shr eax,1F                          |
0019743E | 03C2                                      | add eax,edx                         |
00197440 | 6BC0 2B                                   | imul eax,eax,2B                     |
00197443 | 8BD1                                      | mov edx,ecx                         |
00197445 | 2BD0                                      | sub edx,eax                         |
00197447 | 75 04                                     | jne NS.19744D                       |
00197449 | 85C9                                      | test ecx,ecx                        |
0019744B | 75 02                                     | jne NS.19744F                       |
0019744D | B3 01                                     | mov bl,1                            |

0019744F | 8B4D E4                                   | mov ecx,dword ptr ss:[ebp-1C]       |
00197452 | B8 C94216B2                               | mov eax,B21642C9                    |
00197457 | F7E9                                      | imul ecx                            |
00197459 | 03D1                                      | add edx,ecx                         |
0019745B | C1FA 04                                   | sar edx,4                           |
0019745E | 8BC2                                      | mov eax,edx                         |
00197460 | C1E8 1F                                   | shr eax,1F                          |
00197463 | 03C2                                      | add eax,edx                         |
00197465 | 6BC0 17                                   | imul eax,eax,17                     |
00197468 | 8BD1                                      | mov edx,ecx                         |
0019746A | 2BD0                                      | sub edx,eax                         |
0019746C | 75 04                                     | jne NS.197472                       |
0019746E | 85C9                                      | test ecx,ecx                        |
00197470 | 75 02                                     | jne NS.197474                       |
00197472 | B3 01                                     | mov bl,1                            |
00197474 | 8B4D EC                                   | mov ecx,dword ptr ss:[ebp-14]       |

00197477 | B8 79787878                               | mov eax,78787879                    |
0019747C | F7E9                                      | imul ecx                            |
0019747E | C1FA 03                                   | sar edx,3                           |
00197481 | 8BC2                                      | mov eax,edx                         |
00197483 | C1E8 1F                                   | shr eax,1F                          |
00197486 | 03C2                                      | add eax,edx                         |
00197488 | 8BD0                                      | mov edx,eax                         |
0019748A | C1E2 04                                   | shl edx,4                           |
0019748D | 03D0                                      | add edx,eax                         |
0019748F | 8BC1                                      | mov eax,ecx                         |
00197491 | 2BC2                                      | sub eax,edx                         |
00197493 | 75 04                                     | jne NS.197499                       |
00197495 | 85C9                                      | test ecx,ecx                        |
00197497 | 75 02                                     | jne NS.19749B                       |
00197499 | B3 01                                     | mov bl,1                            |
0019749B | 8B4D C0                                   | mov ecx,dword ptr ss:[ebp-40]       |

0019749E | B8 ED73484D                               | mov eax,4D4873ED                    |
001974A3 | F7E9                                      | imul ecx                            |
001974A5 | C1FA 04                                   | sar edx,4                           |
001974A8 | 8BC2                                      | mov eax,edx                         |
001974AA | C1E8 1F                                   | shr eax,1F                          |
001974AD | 03C2                                      | add eax,edx                         |
001974AF | 6BC0 35                                   | imul eax,eax,35                     |
001974B2 | 8BD1                                      | mov edx,ecx                         |
001974B4 | 2BD0                                      | sub edx,eax                         |
001974B6 | 0F85 5AFFFFFF                             | jne NS.197416                       |
001974BC | 85C9                                      | test ecx,ecx                        |
001974BE | 0F84 52FFFFFF                             | je NS.197416                        |
001974C4 | 84DB                                      | test bl,bl                          |
001974C6 | 0F85 4AFFFFFF                             | jne NS.197416                       |

```

Buraları incelediğimizde belli bir takım matematiksel işlemler yapıldığı görülmekte. Bu işlemler seri numaranın 4 ayrı bölümünden üretilen değerler için yapılmakta. Her bölüm birbirinden ufak farklılıklar içermekte. Bir parçanın işlemi sonucunda iki değer kontrol edilmekte. Başarısız durumlar ecx'in 0 olması, eax veya edx in ise 0'dan farklı değere sahip olması. Bu durumlar `bl`'yi bire eşitleyecek ve en sonda "197416" yerine atlama yaptıracak. Bu atlanılan yer, seri numaranın başarısız olduğunu gösteren hata penceresinin akışının olduğu yerdir. Yukarıda gösterilmedi ama öyle olduğunu bilin. 

Burada adım adım giderken, `x32dbg` yazılımında ilgili yerleri düzenlerseniz (örneğin `jne` yerine `je` yazmak, `edx`'e 0 yazmak) başarılı şekilde ilerlemiş olursunuz. İnternet kapalıyken bu düzenlemeler yapıldığında `NS` yazılımı bilgileri bir formatta `regedit`'e kaydettikten sonra kapanacak ve tekrar açıldığında tam sürüm olduğu görülecektir. Bizim amacımız bu noktada `patch` yapmak değildir. Biz bu noktadan sonra `keygen` yapmayı hedefliyoruz. Eğer keygen yaptıktan sonra sunucu kontrolü gerektiği çıkarsa sadece o nokta için `patch` yapacağız. Normalde bu gibi durumlarda tek `patch` işlemi yapılır. Burada eğitim amacıyla iki özelliği birlikte vermeyi seçiyoruz. `Patch` gerekli mi değil mi diye bakmadan `keygen` yazılımımızı yapacağız.

Yukarıdaki `ASM` komutlarını C diline dökelim. Sağlıklı genel geçer bir C fonksiyonu olmayacak ama Windows altında Mingw64/clang ile istediğimiz gibi çalışmaktadır.

```c

static bool verify_blocks(const int32_t blocks[])
{
    int64_t eax;
    int32_t ecx, edx;

    // Block 1

    ecx = blocks[0];
    eax = (int64_t) ecx * 0x2FA0BE83;
    edx = (int32_t) (eax >> 32);
    edx = edx >> 3; // sar
    eax = ((int32_t)(*(uint32_t*)&edx >> 0x1F) + edx) * 0x2B;
    edx = ecx - (int32_t) eax;

    if (edx != 0)
    {
        return false;
    }

    // Block 2

    ecx = blocks[1];
    eax = (int64_t) ecx * 0xB21642C9;
    edx = (int32_t) (eax >> 32);
    edx = edx >> 4; // sar
    eax = ((int32_t)(*(uint32_t*)&edx >> 0x1F) + edx) * 0x17;
    edx = ecx - (int32_t) eax;

    if (edx != 0)
    {
        return false;
    }

    // Block 3

    ecx = blocks[2];
    eax = (int64_t) ecx * 0x78787879;
    edx = (int32_t) (eax >> 32);
    edx = edx >> 3; // sar
    eax = (int32_t)(*(uint32_t*)&edx >> 0x1F) + edx;
    edx = (((int32_t) eax) << 4) + (int32_t) eax;
    eax = ecx - edx;

    if (eax != 0)
    {
        return false;
    }

    // Block 4

    ecx = blocks[3];
    eax = (int64_t) ecx * 0x4D4873ED;
    edx = (int32_t) (eax >> 32);
    edx = edx >> 4; // sar
    eax = ((int32_t)(*(uint32_t*)&edx >> 0x1F) + edx) * 0x35;
    edx = ecx - (int32_t) eax;

    if (edx != 0)
    {
        return false;
    }

    return true;
}

```

Evet, elimizde iki fonksiyon oldu. Bir tanesi seri numaraları sayısal değere dönüştüren bir fonksiyon, diğeri ise geçerliliği test eden fonksiyon. Geçerlilik testini yapan fonksiyona bakarsak, her bir seri numarası bölümünün bağımsız değerlendirildiği görülmektedir. Halbuki birbirleriyle olan ilişkileri de algoritmaya katılabiliyor. Bu yazılımında böyle bir durum göze çarpmadı.

Şimdi bir C fonksiyonu yazacağız. Rastgele seri numarası üretsin, bu seri numaranın 4 bölümünü sırayla `calculate_block` fonksiyonundan geçirelim. Bu fonksiyondan aldığımız değerleri ise `verify_blocks` fonksiyonuna geçirelim. 

```c

static const char *find_a_serial_with_brute_force(void)
{
    int32_t blocks[4] = { 0 };
    static char serial[] = "XXXXXXXXXXXXXXXXXXXX";
    const char *F4C0 = "WYOP3B2VU7XM8CNR9E5T6SA0IZ4KLFQGHDJ1";

    srand((unsigned int)time(NULL));

    for (;;)
    {
        for (int idx = 0; idx < 20; idx++)
        {
            serial[idx] = F4C0[rand() % 36];
        }
                
        calculate_block(&blocks[0], &serial[0]);
        calculate_block(&blocks[1], &serial[5]);
        calculate_block(&blocks[2], &serial[10]);
        calculate_block(&blocks[3], &serial[15]);

        if (verify_blocks(blocks))
        {
            return serial;
        }
    }
}

```

Bu fonksiyondan dönecek olan adres geçerli bir seri numarası adresidir. (Bu fonksiyon başka şekillerde yazılabilirdi.) Bu fonksiyondan dönen seri numarasını '-' karakterleriyle göstermek için `main` fonksiyonuna aşağıdaki kod yazılabilir.

```c

char p_readable[24] = {0};
const char *p_sn = find_a_serial_with_brute_force();

sprintf(p_readable, "%.5s-%.5s-%.5s-%.5s", &p_sn[0], &p_sn[5], &p_sn[10], &p_sn[15]);
puts(p_readable);

```

İşte `Keygen` hazır :)

Sayısal değerleri değiştirdiğim için bu algoritmadan geçerli bir seri numarası üretilir mi bilmiyorum :D Ama gerçek halinde bu yöntemle üretim sağlanabiliyordu.

Tabi interneti açıyoruz ve denemeye çalışırken tam sürümden bir süre sonra çıkıyoruz. Sahte kayıt olduğu anlaşılıyor ve doğru kayıt yapmamız isteniyor. Demek ki belirli periyotlarla veya en azından bir kere `regedit`'e kayıt edilen bilgiler ilgili sunucuda sorgulanıyor. Biz sorgu arasına girebiliriz belki. İmzalar, şifrelemeler vb. durumlar bizi uğraştırabilir. Şimdilik amacımız `Patch` gerekliliği olduğundan bir sonraki bölüme atlıyoruz.

Buraya kadar `x32dbg` yazılımından aldığım çıktıları buraya koyduğumdan bazı adres değerlerinin uyuşmadığı görülebilir. Çünkü `x32dbg` yazılımında bir uygulamayı tekrar açtığımda bana gösterilen adresler değişebilmektedir. Dikkat ederseniz son 4 basamak yani 2 baytın aynı olduğu görülebilir. Bu nedenle bu farklılıkları düzenlemekle uğraşmadım. Önemli olan bunlar değil, işin mantığını aktarabilmek ve deneyerek öğrenmeniz.

## Patch Yapalım

Genelde `Patch` yazılımı varsa `Keygen` yazılımına gerek duyulmaz. Sonuçta `Patch` ile de `Keygen` için baktığımız baytları değiştirebiliriz. Eğitim olarak ilerlediğimizden en son üreteceğimiz yazılım hem `Patch` hem `Keygen` olacaktır. Bu bölüm aşırı basittir. 

`NS` yazılımına `Patch` yapmamız gerekmektedir çünkü ilgili sunuculara istek yapmakta ve kendi veri tabanlarıyla karşılaştırmaktadır. Geri dönen cevap da arzu edilir ki yakalanmamızı sağlar. Burada yapılacak yöntemler çeşitlidir. Yerel bir sunucu davranışı taklit edilebilir. Yazılım kendi sunucularına bağlanıyor sanabilir. Sunuculara istek atan yazılım sunucudan aldığı cevapla işlem yaptıktan sonra ilerleyişi değiştirilebilir. Ya da sunucuya istek atmasının önüne geçilebilir. Biz bu yazıda sunucuya istek hiç atmamasını sağlayacağız. `NS` yazılımı geçerli bir seri numarası girildiğinde otomatik olarak kendisini tam sürüme yükseltmektedir. Biz `patch` işlemi ile sadece sunucuya istek atmanın önüne geçebilirsek `Keygen` bölümünde yaptıklarımız boşa gitmeyecektir :)

Senaryomuzu açıklayalım. Öncelikle `x32dbg` ile yazılım açılacak tüm kayıt alanları doldurulacak. `Keygen` bölümünde seri numarası kontrol yerinin sonuna `breakpoint` koyacağız. Seri numarası alanı için `Keygen` yazılımından ürettiğimiz seri numarasını kullanacağız. İnternet açık olacak ve `Wireshark` yazılımı açık olacak. Sonra adım adım `Step over`butonu ile yazılımı takip edeceğiz. 

Kolaylık olsun diye tarayıcınız veya herhangi internete bağlı uygulamanız varsa çıkış yapın. Adım adım `NS` yazılımında ilerlerken hangi `call` sonrası `Wireshark` da sunucu isteği atıldığını bulun ve `x32dbg` yazılımında ilgili yere `breakpoint` koyun. Sonra yazılımı `x32dbg` ile tekrar açın ilgili yere gelin bu sefer `Step over` butonunun solundaki `Step into` butonu ile fonksiyonun içine girin. Yazılımı analiz edin ve başka bir işlem yapmayan sadece istek atan ve cevabbı analiz eden fonksiyon çağrısını bulun. 

Bu adımlar denemeye dayalı olduğu için düz yazı olarak geçildi. 

`NS` yazılımında bulunan yerde sunucuya istek atan fonksiyon çağrımından sonra kayıt defteri işlemleri de vardı. Fonksiyon çağrımı sonrası `eax` yazmacı kontrol ediliyor ona göre başarılılık veya başarısızlık ile alakalı işlemler yapılıyordu. İlgili fonksiyon çağrımında `breakpoint` varken interneti kapatarak tekrar denendi. İlgili fonksiyon çağrımına gelindiğinde `Step over`sonrası `eax` yazmacına 4 değeri yazıldığı görüldü. Yazılım akışına devam ettiğinde `NS` yazılımı kapandı. Tekrar açıldığında ise kaldığı yerden tam sürüme devam etti. Bu nedenle 5 bayt olan ilgili fonksiyon çağrımı yerini 5 bayt yer alacak `mov eax, 4` ile değiştirmek en akıllıca çözüm olacaktır.

İlgili fonksiyon çağrımı yeri:

```asm

01385869 | E8 E2F4FFFF                               | call <NS.sub_1384D50>               | burada internet kontrolu var
0138586E | 8945 D8                                   | mov dword ptr ss:[ebp-28],eax       |

```

"01385869" adresinin olduğu satıra fare ile tıkladıktan sonra boşluk tuşuna basıyoruz. Karşımıza gelen ekran ASM düzeyinde değişiklik yapmamızı söyleyen bir ekran olacaktır. Orada `call 0x01384D50` komutu olacak. Yukarıda görüldüğü gibi bayt karşılığı `E8E2F4FFFF` olan bu komut yerine `mov eax, 4` komutunu çıkan ekrana yazıyoruz:

![Patch işlemi](pictures/x32dbg_ns_patch_ss.png)

`OK` dedikten sonra interneti açıp aynı akışı tekrar yapıyoruz. `NS` yazılımı kapanacak ve tekrar açtığımızda tam sürüm görülecektir. `x32dbg` yazılımında bu değişikliği kalıcı hale getirebiliriz ama biz C dilinde `Patch` yazılımı kısmını yapacağız.

Nasıl yapılacağını tahmin ediyor musunuz? Dosyayı aç, `E8E2F4FFFF` bayt dizisini bul ve yukarıda `mov eax, 4` komutunun bayt dizilimi olan `B804000000` ile yer değiştir. Tabi, "`E8E2F4FFFF` başka bir dizilime denk geliyor mu? Başka yerde çağrılıyor mu?" gibi sorular bulunmakta. Bunu bir hex görüntüleyici ile (notepad++ + hex plugin ile denedim) arayıp bakabilirsiniz. Bu yazılımda bu bayt dizilimine sahip ilk yer değiştirmemiz gereken yer olduğu görülmüştür.

Gelelim kod kısmına...

Öncelikle bizim yazılım bu eylemi yapabilmesi için yöneticilik izni ile çalışması gerekmektedir. Bu nedenle yönetici izniyle mi çalıştırıldı diye bir fonksiyon gerekmektedir. Eğer yöneticilik izni ile çalışmadı ise ekrana bu durumu gösterdikten sonra bir dahaki açılışın kullanıcı tarafından yöneticilik izni ile gerçekleştireleceğini umacağız :)

İlgili fonksiyon:

```c

static bool is_run_as_administrator(void)
{
    BOOL fIsRunAsAdmin = FALSE;
    PSID pAdministratorsGroup = NULL;

    if (AllocateAndInitializeSid(
        &((SID_IDENTIFIER_AUTHORITY){SECURITY_NT_AUTHORITY}), 
        2U, 
        SECURITY_BUILTIN_DOMAIN_RID, 
        DOMAIN_ALIAS_RID_ADMINS, 
        0, 0, 0, 0, 0, 0, 
        &pAdministratorsGroup))
    {
        (void)CheckTokenMembership(NULL, pAdministratorsGroup, &fIsRunAsAdmin);
    }

    if (pAdministratorsGroup)
    {
        (void)FreeSid(pAdministratorsGroup);
    }

    return fIsRunAsAdmin;
}

```

Bu fonksiyondan doğru değerini alırsak, kullanıcı eğer ekrana tıklarsa bir dosya seçme ekranı açılsın, bir `exe` seçmesi gerektiğini filtreleyelim, sonra dosyada ilgili bayt dizilimini bulalım değiştirelim yeni dosya olarak yazalım eskisini silelim, kullanıcı tekrar tıklarsa `keygen` aşamasına geçilsin...

Fazla kontrol yok çok da önemsenmedi yazılım yazılırken.

Tıklama kontrolü için fonksiyon:

```c

static void detect_mouse_click(void)
{
    DWORD Events;
    CONSOLE_CURSOR_INFO cci = {.dwSize = 25, .bVisible = FALSE};
    INPUT_RECORD InputRecord;
    HANDLE hin = GetStdHandle(STD_INPUT_HANDLE);
    HANDLE hout = GetStdHandle(STD_OUTPUT_HANDLE);

    (void)FlushConsoleInputBuffer(hin);

    (void)SetConsoleCursorInfo(hout, &cci);
    (void)SetConsoleMode(hin, ENABLE_EXTENDED_FLAGS);
    (void)SetConsoleMode(hin, ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT);

    for (;;)
    {
        ReadConsoleInput(hin, &InputRecord, 1, &Events);

        if ((MOUSE_EVENT == InputRecord.EventType) &&
            (InputRecord.Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED))
        {
            break;
        }
    }
}

```

Yazılımın `Patch` edildiği fonksiyon:

```c

static bool program_patch(void)
{
    bool ret = false;

    WCHAR file_full_path[512] = {0};
        OPENFILENAMEW ofn = {
        .lStructSize = sizeof(ofn),
        .hwndOwner = NULL,
        .lpstrFile = file_full_path,
        .nMaxFile = sizeof(file_full_path),
        .lpstrFilter = L"NS.exe\0*.EXE\0",
        .nFilterIndex = 1,
        .lpstrFileTitle = NULL,
        .nMaxFileTitle = 0,
        .lpstrInitialDir = NULL,
        .Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST,
    };

    print_text("Patch", "Find NS.exe", "Select it!");

    if (GetOpenFileNameW(&ofn))
    {
        FILE *tmp_file;

        if ((tmp_file = _wfopen(file_full_path, L"rb+")) != NULL)
        {
            (void)fseek(tmp_file, 0, SEEK_END);
            size_t len = (size_t)ftell(tmp_file);
            (void)fseek(tmp_file, 0, SEEK_SET);

            unsigned char *p_buff = malloc(len);

            (void)fread(p_buff, 1U, len, tmp_file);

            for (size_t idx = 0U; idx < len - 4U; idx++)
            {
                if (0 == memcmp(p_buff + idx, "\xe8\xe2\xf4\xff\xff", 5U))
                {
                    (void)memcpy(p_buff + idx, "\xb8\x04\x00\x00\x00", 5U);
                    ret = true;

                    break;
                }
            }

            (void)fclose(tmp_file);

            if (true == ret)
            {
                _wremove(file_full_path);

                if ((tmp_file = _wfopen(file_full_path, L"wb")) != NULL)
                {
                    (void)fwrite(p_buff, 1U, len, tmp_file);
                    (void)fclose(tmp_file);
                }

                print_text("Patch", "Succesfull Patch!", "Click to generate serial numbers");
            }
        }
    }

    if (false == ret)
    {
        print_text("Patch couldn't be done!", "The program will close!", "Click to close");
    }

    detect_mouse_click();

    return ret;
}

```

`Patch` için gerekli fonksiyonlar da bitti. Son olarak 8 bit müzik ekleyelim, güzelleşelim. Kaynak kodun son halini de paylaşalım.

## 8 Bit Müzik Keyfi

Çalıştığım şirkette nedense çoğunlukla "Ali Güven - Yolcu" çalmakta. YouTube'da Türkçe bazı şarkıların 8-bit hallerini paylaşan bir kanalda ([Tıkla](https://www.youtube.com/user/8bitturkishmusic)) bu şarkının da 8-bit hâli olduğundan 8-bit müzik için seçilen şarkı bu oldu :)

Öncelikle `ontiva.com` sitesinden şarkının son 1 dakikası indirilir. `conversiontools.io` sitesinden ise en düşük "wav" formatına dönüştürülür. Küçük bir C programı vasıtasıyla, "wav" dosyası bayt bayt okunur ve "\xXX" formatı ile tüm baytları bir dosyaya yazılır. Bu tüm çıktıyı biz bir dizide tutacağız. Sonra Windows API sayesinde geçici dosya olarak "wav" dosyasını oluşturacağız. Sonra yine bir Windows API fonksiyonu olan `PlaySound` fonksiyonuna bu dosyanın yolunu vereceğiz.

## Kaynak Kod

Derlenebilir olarak `NS` yazılımı için yapılan `keygen` ve `patch` yazılımının kaynak koduna aşağıdaki linkten erişebilirsiniz. Kaynak kodun en üstünde nasıl derleneceği ile alakalı yorum satırı bulunmaktadır

[NS patch and keygen](https://gitlab.com/enes1313/bir-yazilimi-kirmak-sw-cracking/-/raw/master/ns_patch_and_keygen.c)

